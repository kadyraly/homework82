const express = require('express');
const TrackHistory = require('../model/TrackHistory');
const User = require('../model/User');
const Tracks = require('../model/Tracks');
const router = express.Router();


const createRouter = () => {
    router.get('/', (req, res) => {
        TrackHistory.find()
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));

    });

    router.post('/', async (req, res) => {
        const tokenId = req.get('Token');
        const user = await User.findOne({token: tokenId}).select('_id');
        let historyData = req.body;
        if (user) {
            const track = req.body.track;
            const isTrack = await Tracks.find({_id: track}).select('_id');
            if (isTrack) {
                historyData.user = user._id;
                historyData.datetime = new Date();
                const history = new TrackHistory(historyData);
                res.send({history});
            } else {
                res.sendStatus(400);
            }
        } else {
            res.sendStatus(401)
        }

    });

    return router;
};

module.exports = createRouter;