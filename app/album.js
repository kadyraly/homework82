const express = require('express');

const multer = require('multer');
const path = require('path');
const Album = require('../model/Album');
const nanoid = require('nanoid');
const  config = require('../config');
const router = express.Router();

const ObjectId = require('mongodb').ObjectId;
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {

        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const createRouter = () => {
    // Product index
    router.get('/', (req, res) => {
        if (req.query.artists) {
            Album.find({author: req.query.artists})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }

    });

    // Product create
    router.post('/', upload.single('image'), (req, res) => {

        const albumData = req.body;


        if (req.file) {
            albumData.image = req.file.filename;
        } else {
            albumData.image = null;
        }

        const album = new Album(albumData);
        album.save()
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });
    router.get('/:id', (req, res) => {
        Album
            .findOne({_id: req.params.id})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404)
            })
            .catch(() => res.sendStatus(500));
    });

    return router;
};

module.exports = createRouter;