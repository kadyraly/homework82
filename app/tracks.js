const express = require('express');


const path = require('path');
const Track = require('../model/Tracks');
const nanoid = require('nanoid');
const  config = require('../config');
const router = express.Router();

const ObjectId = require('mongodb').ObjectId;

const createRouter = () => {
    // Product index
    router.get('/', (req, res) => {
        if(req.query.album) {

            Track.find({album: req.query.album})
                .then(results => res.send(results))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', (req, res) => {
       const trackData = req.body;

       const track = new Track(trackData);

       track.save().then(track => {
           res.send(track)
       })
    });

    return router;
};

module.exports = createRouter;