const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const artists = require('./app/artists');
const app = express();
const albums = require('./app/album');
const tracks = require('./app/tracks');
const users = require('./app/users');
const trackHistories = require('./app/trackHistory');
const port = 8000;

app.use(cors());

app.use(express.json());

app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
db.once('open', () => {
    console.log('Mongoose was loaded!');

    app.use('/artists', artists());
    app.use('/albums', albums());
    app.use('/tracks', tracks());
    app.use('/users', users());
    app.use('/track_history', trackHistories());

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});